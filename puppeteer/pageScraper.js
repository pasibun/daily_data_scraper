const scraperObject = {
	url: 'https://www.amazon.nl/Epoxyhars-verf-mica-poeder-cosmeticapigment/dp/B09W63KT3H/ref=sr_1_2?__mk_nl_NL=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3Q78BZLCL4072&keywords=Epoxyhars+verf+mica+poeder+5g&qid=1669315981&qu=eyJxc2MiOiIzLjMwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sprefix=epoxyhars+verf+mica+poeder+5g%2Caps%2C92&sr=8-2',
	async scraper(browser) {
		let page = await browser.newPage();
		console.log(`Navigating to ${this.url}...`);
		await page.goto(this.url);
		const cookie = await page.$("#sp-cc-accept")
		cookie.click();

		const price = await getTextOfClass(page, "[class='a-offscreen']");
		console.log(price);

		const voorraad = await getTextOfClass(page, "[class='a-size-medium a-color-success']");
		console.log(voorraad);
	}
}


async function getTextOfClass(page, className) {
	const result = await page.$(className);
	return await(await result.getProperty('textContent')).jsonValue();
}
module.exports = scraperObject;
